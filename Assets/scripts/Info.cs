﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Info : MonoBehaviour
{
    [SerializeField] bool pinned = false;

    public static Vector3 WorldToScreenPointProjected(Camera camera, Vector3 worldPos)
    {
        Vector3 camNormal = camera.transform.forward;
        Vector3 vectorFromCam = worldPos - camera.transform.position;
        float camNormDot = Vector3.Dot(camNormal, vectorFromCam);
        if (camNormDot <= 0)
        {
            // we are behind the camera forward facing plane, project the position in front of the plane
            Vector3 proj = (camNormal * camNormDot * 1.01f);
            worldPos = camera.transform.position + (vectorFromCam - proj);
        }
        return camera.WorldToScreenPoint(worldPos);
    }


    [SerializeField] public string Title;
    [Multiline(5)][SerializeField] public string Description;
    [SerializeField] RectTransform icon;
    
    void Start()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => { Click(); });
        icon.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (pinned) return;

        icon.position = WorldToScreenPointProjected(Camera.allCameras[0], transform.position);
    }

    public void Click()
    {
        GameManager.Instance().titleText.text = Title;
        GameManager.Instance().infoText.text = Description;

        GameManager.Instance().infoPanel.gameObject.GetComponent<Animator>().SetBool("visible", true);


        GameManager.Instance().plantinfo = false;

        if (pinned)
            GameManager.Instance().tpsCamera.GetComponent<CameraController>().ChangeTarget(GameManager.Instance().tpsCamera.transform.parent);
        else
            GameManager.Instance().tpsCamera.GetComponent<CameraController>().ChangeTarget(transform);
    }
}
