﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;

public class GameManager : MonoBehaviour
{
    #region Singleton and playerData json
    public static GameManager Instance() { return _instance; }
    private static GameManager _instance;

    public bool plantinfo = false;
    void Awake()
    {
        if (!_instance) _instance = this;
        DontDestroyOnLoad(this);
    }
    #endregion

    [SerializeField] TurnOffBehaviour turnOffBehaviour;
    [SerializeField] GameObject buildingA, buildingB, plane;
    [SerializeField] public Camera tpsCamera, arCamera;
    [SerializeField] public RectTransform infoPanel;
    [SerializeField] public Text titleText, infoText;
    [SerializeField] ToggleSwitch BuildingSwitch, ModeSwitch, InfoSwitch;

    [SerializeField] RectTransform iconsA, iconsB;
    public void ChangeBuilding()
    {
        buildingA.SetActive(!BuildingSwitch.isOn);
        buildingB.SetActive(BuildingSwitch.isOn);
        iconsA.gameObject.SetActive(!BuildingSwitch.isOn);
        iconsB.gameObject.SetActive(BuildingSwitch.isOn);

        titleText.text = BuildingSwitch.isOn ? buildingB.GetComponent<Info>().Title : buildingA.GetComponent<Info>().Title;
        infoText.text = BuildingSwitch.isOn ? buildingB.GetComponent<Info>().Description : buildingA.GetComponent<Info>().Description;
        plantinfo = true;

        tpsCamera.GetComponent<CameraController>().ChangeTarget(tpsCamera.transform.parent);
    }
    public void ChangeInfo()
    {
        //if (plantinfo)
            infoPanel.gameObject.GetComponent<Animator>().SetBool("visible", false);
        /*
        else
        {
            titleText.text = BuildingSwitch.isOn ? buildingB.GetComponent<Info>().Title : buildingA.GetComponent<Info>().Title;
            infoText.text = BuildingSwitch.isOn ? buildingB.GetComponent<Info>().Description : buildingA.GetComponent<Info>().Description;
            tpsCamera.GetComponent<CameraController>().ChangeTarget(tpsCamera.transform.parent);
            plantinfo = true;
        }
        */
    }
    public void ChangeMode()
    {
        arCamera.enabled = !ModeSwitch.isOn;
        tpsCamera.enabled = ModeSwitch.isOn;
        plane.SetActive(ModeSwitch.isOn);
        tpsCamera.GetComponent<CameraController>().enabled = ModeSwitch.isOn;

        arCamera.GetComponent<VuforiaBehaviour>().enabled = !ModeSwitch.isOn;
        foreach (MeshRenderer mr in turnOffBehaviour.gameObject.GetComponentsInChildren<MeshRenderer>(true))
        {
            mr.enabled = ModeSwitch.isOn;
        }

        tpsCamera.GetComponent<CameraController>().ChangeTarget(tpsCamera.transform.parent);
    }
}
