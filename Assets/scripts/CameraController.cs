﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    [SerializeField][Range(45, 89)] float Y_MAX = 85f; //константа
    [SerializeField][Range(1, 44)] float Y_MIN = 20f;

    public Transform target;
    public float distance = 5.0f;
    public float sensX = 4;
    public float sensY = 4;
    public float distMin = 1;
    public float distMax = 10;

    [SerializeField] float Sensivity;
    [SerializeField] Text debugText;
    float initialFingersDistance;

    float curX = 0;
    float curY = 0;

    Vector3 targetPos;
    bool moving = false;
    void Update()
    {   
        if (moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, 100 * Time.deltaTime);
            if (transform.position == targetPos) moving = false;
            return;
        }


        if (Input.touchCount == 2)
        {
            if (Input.touches[0].phase == TouchPhase.Began || Input.touches[1].phase == TouchPhase.Began && !EventSystem.current.IsPointerOverGameObject(Input.touches[1].fingerId))
            {
                initialFingersDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
            }
            else if (Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved)
            {
                float currentFingersDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
                float scaleFactor = currentFingersDistance / initialFingersDistance;
                distance += (1-scaleFactor) * 30 * (distance * 0.3f / 200f);
                distance = Mathf.Clamp(distance, distMin, distMax);

                debugText.text += scaleFactor + "zooming \n";
            }
            return;
        }

        else if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                curX += (touch.deltaPosition.x * Sensivity);
                curY += (-touch.deltaPosition.y * Sensivity);
            }
            else return;
        }
        #region mouse input
        curX += Input.GetAxis("Horizontal") * sensX;
        curY += Input.GetAxis("Vertical") * sensY;
        
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
            distance -= Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"));
        #endregion

        curY = Mathf.Clamp(curY, Y_MIN, Y_MAX);
        distance = Mathf.Clamp(distance, distMin, distMax);
    }

    private void LateUpdate()
    {
        if (!moving)
        {
            Vector3 dir = new Vector3(0, 0, -distance);
            Quaternion rot = Quaternion.Euler(curY, curX, 0);
            transform.position = target.position + rot * dir;
            transform.LookAt(target);
        }
    }

    public void ChangeTarget(Transform t)
    {
        target = t;

        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rot = Quaternion.Euler(curY, curX, 0);
        targetPos = target.position + rot * dir;

        moving = true;
    }
}
