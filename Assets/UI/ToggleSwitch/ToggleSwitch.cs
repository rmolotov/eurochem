﻿using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
    [Serializable]
    public class ToggleSwitchEvent : UnityEvent<bool>
    { }
    public class ToggleSwitch : Selectable, IPointerClickHandler, ISubmitHandler, ICanvasElement
    {
        public ToggleSwitchEvent onValueChanged = new ToggleSwitchEvent();
        private Animator _animator;

        [Tooltip("Is the toggle currently on or off?")]
        [SerializeField]
        private bool m_IsOn;
        public bool isOn
        {
            get { return m_IsOn; }

            set
            {
                Set(value);
            }
        }

        void Start()
        {
            _animator = GetComponent<Animator>();
        }


        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            InternalToggle();
        }

        public virtual void OnSubmit(BaseEventData eventData)
        {
            InternalToggle();
        }

        public virtual void Rebuild(CanvasUpdate executing)
        {
#if UNITY_EDITOR
            if (executing == CanvasUpdate.Prelayout)
                onValueChanged.Invoke(m_IsOn);
#endif
        }

        public virtual void LayoutComplete()
        { }
        public virtual void GraphicUpdateComplete()
        { }

        private void InternalToggle()
        {
            if (!IsActive() || !IsInteractable())
                return;

            isOn = !isOn;
        }

        void Set(bool value, bool sendCallback = true)
        {
            if (m_IsOn == value)
                return;

            // if we are in a group and set to true, do group logic
            m_IsOn = value;
            _animator.SetBool("value", value);

            // Always send event when toggle is clicked, even if value didn't change
            // due to already active toggle in a toggle group being clicked.
            // Controls like Dropdown rely on this.
            // It's up to the user to ignore a selection being set to the same value it already was, if desired.
            if (sendCallback)
            {
                UISystemProfilerApi.AddMarker("Toggle.value", this);
                onValueChanged?.Invoke(m_IsOn);
            }
        }
    }
}
